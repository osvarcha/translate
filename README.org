#+title: Translate for Osvarcha
#+author: Osvarcha

Este repositorio es una recopilacion de mis traducciones hechas por mi
para practicar diferentes temas, tanto libros de especialidad, como
documentacion y referencias.

Al principio solo traducia y lo guarda en mis notas personales pero
decidi unirlos ya que las traducciones son de una misma fuente.

Las traducciones estan en formato Org lo cual se podria exportar a
latex y posteriormente a PDF.

* Que traducciones hay?

Por el momento solo pude unir las siguientes traducciones:

- [[file:Guile/Manual-guile-es.org][Manual de Referencia de Guile]], una traduccion en partes que espero
  sirva de ayuda a cualquier persona que este interesado en el mundo
  de scheme y el proyecto GNU.

